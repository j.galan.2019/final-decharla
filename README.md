# Final DeCharla



# ENTREGA CONVOCATORIA MAYO

# ENTREGA DE PRÁCTICA

## Datos

* Nombre: Jorge Galán Fernández
* Titulación: Ingeniería en Tecnologías de la Telecomunicación
* Cuenta en laboratorios: galan
* Cuenta URJC: j.galan.2019
* Video básico (url): https://youtu.be/sD9KZ-9iB3s
* Video parte opcional (url): https://youtu.be/iS9hP1RkoHM
* Despliegue (url):  jorgegalan12.pythonanywhere.com
* Contraseñas: batman, superman, flash
* Cuenta Admin Site: jorge/jorge

## Resumen parte obligatoria

La siguiente aplicación es una plataforma de chat que permite a los usuarios comunicarse en tiempo real. Al iniciar sesión, los usuarios son redirigidos a la página de inicio (HOME), donde se encuentran diversas opciones de navegación.

En la barra de navegación, se disponen los siguientes enlaces: HOME, ADMIN, CONFIGURACIÓN, AYUDA y CHATS. Al hacer clic en CHATS, se despliega dinámicamente una lista de salas de chat disponibles.

En la página HOME encontraremos, si existen, las salas de chat disponibles,, en ellas podemos ver título, descripción, mensajes
        totales, última hora de acceso, mensajes no leídos (entre otras cosas). También podemos ver en cada sala un botón de acceso para el chat
        normal, otro para el chat dinámico, y otro para acceder al JSON.

Al acceder a una sala de chat, se muestra una lista de mensajes previamente enviados. Cada mensaje incluye el nombre del usuario que lo escribió (por defecto, "Anónimo"), la hora de envío y el contenido del mensaje. En la parte superior de la página se encuentra un formulario donde los usuarios pueden redactar y enviar mensajes. Si se desea adjuntar una imagen, se puede proporcionar la URL correspondiente y seleccionar el botón para enviar la foto.

Al acceder a una sala de chat dinámica, tenemos la misma funcionalidad que en la sala normal de chat pero los mensajes se refrescan cada 30segundos

Además, existe la posibilidad de utilizar los recursos de las salas de chat estáticas para realizar una operación de "PUT". Esta operación permite agregar una serie de mensajes en formato "XML" a la sala de chat seleccionada. Sin embargo, es importante destacar que esta acción solo puede llevarse a cabo si se incluyen las Cookies correspondientes a la sesión en las cabeceras de la solicitud "PUT" y su CSRF Token con la cabecera X-CSRFToken obligtoriamente.

En cuanto a la estructura del "XML", se requiere incluir los siguientes campos:

- Cabecera en formato `XML`.
- Campo `messages`: Este campo engloba todos los mensajes que se desean agregar.
- Campo `message`: Cada instancia de este campo representa un mensaje específico que se desea incluir. Además, se debe proporcionar la etiqueta "isimg", la cual solo puede tener el valor de "True" si el mensaje es una imagen, o "False" si el mensaje es un texto.
- Campo `text`: Este campo contiene el texto del mensaje o la URL de la imagen, dependiendo del caso.

Además de las funcionalidades de chat, en la parte derecha de la página hay un cuadro de usuario que muestra el nombre de usuario actualmente activo. También se encuentra un botón para crear una nueva sala de chat, redirigiendonos al recurso para crear sala yeando la sala mediante un formulario de nombre, otro de descripción y un boton para crearla.

Tambien tenemos el apartado configuración donde podemos configurar ciertos apartados de la aplicación como tipo de fuente,
tamaño de fuente, color de fondo y nombre de usuario mediante distintos formularios

Tambien existe el apartado ayuda que mediante varios deplegables nos da intrucciones de uso de la página


En resumen, esta aplicación de chat ofrece a los usuarios la posibilidad de participar en salas de chat, enviar mensajes de texto y compartir imágenes, además de ciertas personalizaciones y funcionalidades para hacer la experiencia más amena, cómoda y eficiente.

## Lista partes opcionales

* Obtención del fav-icon
* Inclusión de Descripción de cada sala
* Inclusión de LogOut: Se añade la función de logOut para cerrar la sesion en el recurso /logout
* Función de marcar salas como favoritas : Esto se puede hacer desde "HOME" (recurso /) cuando se nos muestran todas las salas (aparece una estrella vacia si no está marcada y rellena de amarillo si está marcada).
* Inclusión de nuevo recurso /favoritos en el que ver las salas marcadas como favorito
* Distinción de mensajes propios y ajenos : En un Chat los mensajes propios aparecen en amarillo y a la derecha y los ajenos en morado y a la izquierda tanto en salas dinámicas como en estáticas
* Función para cambiar el background: En el apartado configuración, podemos cambiar el background de color mediante un formulario en el que podemos introducir el color (en inglés)
* Logo de la página tanto en Cabecera como pie de página
* Se muestra fecha de último acceso a cada sala en HOME
