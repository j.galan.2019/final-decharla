function getMessages(url, container) {
  fetch(url).then(response => {
  console.log("Response received!", response)
  if (!response.ok) {
    throw new Error("HTTP error " + response.status);
  }
  return response.json();
  }).then(messages => {
    console.log(messages);
    let messages_html = "";
    messages.forEach(message => {
      isoDate = message.date;
      fecha = new Date(isoDate);
      DateFormat = fecha.toLocaleDateString('en-US', {
        month: 'short',
        day: 'numeric',
        year: 'numeric'
      });
      HourFormat = fecha.toLocaleTimeString('en-US', {
        hour: 'numeric',
        minute: 'numeric',
        hour12: true
      });
      date = DateFormat + ', ' + HourFormat;
      message_html = '<div class="message-row align-right">';
      if (message.is_img === false) {
        if (message.session === session) {
          message_html += '<div class="right-bubble">';
          message_html += '<p>' + message.msg + '</p>';
          message_html += '<hr className="my-4">'
          message_html += '<p>'+ "De:" + message.author + '</p>';
          message_html += '<p>' + date + '</p>';
          message_html += '</div>';
        } else {
          message_html += '<div class="left-bubble">';
          message_html += '<p>' + message.msg + '</p>';
          message_html += '<hr className="my-4">'
          message_html += '<p>'+ "De:" + message.author + '</p>';
          message_html += '<p>' + date + '</p>';
          message_html += '</div>';
        }
      } else {
        if (message.session=== session) {
          message_html += '<div class="right-bubble">';
          message_html += '<p><img src="' + message.msg + '" class="img"></p>';
          message_html += '<hr className="my-4">'
          message_html += '<p>'+ "De:" + message.author + '</p>';
          message_html += '<p>' + date + '</p>';
          message_html += '</div>';
        } else {
          message_html += '<div class="left-bubble">';
          message_html += '<p><img src="' + message.msg + '" class="img"></p>';
          message_html += '<hr className="my-4">'
          message_html += '<p>'+ "De:" + message.author + '</p>';
          message_html += '<p>' + date + '</p>';
          message_html += '</div>';
        }
      }
      message_html += '</div>'
      messages_html += message_html;
    });
    container.innerHTML = messages_html;
  })
  }

window.addEventListener("DOMContentLoaded", (event) => {
  console.log("DOM fully loaded and parsed");
  const container = document.querySelector('#messages');
  getMessages(url_messages, container);
  setInterval(() => {
    console.log("Interval");
    getMessages(url_messages, container);
    }, 30 * 1000); // Call every 30 seconds
});
