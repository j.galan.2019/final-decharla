from django.contrib import admin
from .models import Session, Passwords, Chat, ChatMsg, VisitedChats, LikedChats

# Register your models here.


admin.site.register(Session)
admin.site.register(Passwords)
admin.site.register(Chat)
admin.site.register(ChatMsg)
admin.site.register(VisitedChats)
admin.site.register(LikedChats)

