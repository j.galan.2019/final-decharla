from django.urls import path

from . import views

urlpatterns = [
    path('', views.get_home, name='index'),
    path('favicon.ico', views.get_favicon, name='favicon'),
    path('help', views.get_help, name='help'),
    path('configuration', views.get_configuration, name='configuration'),
    path('login', views.login, name='login'),
    path('logout', views.logout, name='logout'),
    path('favourites', views.get_favs, name='favs'),
    path('createChat', views.create_chat, name='createChat'),
    path('<str:chat_name>.json', views.get_json, name='chatJSON'),
    path('<str:chat_name>.dinamica', views.dynamic_chat, name='chatDinamico'),
    path('<str:chat_name>', views.chat, name='chat'),


]
