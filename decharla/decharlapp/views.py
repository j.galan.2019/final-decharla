import json
import random
import string
from django.utils import timezone

from django.shortcuts import render, HttpResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt, ensure_csrf_cookie
import xml.etree.ElementTree as ET
from .models import Session, Passwords, Chat, ChatMsg, VisitedChats, LikedChats


def get_favicon(request):
    favicon = open("favicon.png", "rb")
    ans = favicon.read()
    return HttpResponse(ans)

def get_chat_list():
    chat_list = []
    for chat in Chat.objects.all():
        chat_list.append(chat)
    return chat_list


def get_msg_list(request):
    msg_list = []
    cookie = request.COOKIES.get("session")
    if request.path.endswith('.json'):
        chat_name = request.path.rstrip('.json').split('/')[-1]
    else:
        chat_name = request.path.lstrip("/")
        for msg in ChatMsg.objects.all():
            if msg.chat.name == chat_name:
                msg_list.append(msg)
    return msg_list

def get_footer_nums(request):
    msg_count = 0
    salas_count = 0
    img_count = 0

    for mensaje in ChatMsg.objects.all():
        if mensaje.is_img == False:
            msg_count = msg_count+1
        elif mensaje.is_img == True:
            img_count = img_count+1
    for sala in Chat.objects.all():
        salas_count = salas_count+1
    return msg_count, img_count, salas_count

def get_notread(request):
    array_counters = []
    for sala in Chat.objects.all():
        counter = 0
        cookie = request.COOKIES.get("session")
        visited = VisitedChats.objects.filter(chat=sala,session=Session.objects.get(session=cookie))
        if visited:
            visit = VisitedChats.objects.get(chat=sala,session=Session.objects.get(session=cookie))
            for msg in ChatMsg.objects.filter(chat=sala):
                if visit.access_date < msg.date:
                    counter = counter+1
        else:
            counter = len(ChatMsg.objects.filter(chat=sala))
        array_counters.append(counter)
    return array_counters

def get_chathour(request):
    array_hour = []
    for sala in Chat.objects.all():
        cookie = request.COOKIES.get("session")
        visited = VisitedChats.objects.filter(chat=sala,session=Session.objects.get(session=cookie))
        if visited:
            visit = VisitedChats.objects.get(chat=sala,session=Session.objects.get(session=cookie))
            array_hour.append(visit.access_date)
        else:
            array_hour.append("No se accedio aún")
    return array_hour

def get_username(request):
    cookie = request.COOKIES.get("session")
    sesion = Session.objects.get(session=cookie)
    name = sesion.name
    return name

def get_totalchatmsg(request):
    array_msgs = []
    for sala in Chat.objects.all():
        counter = 0
        for msg in ChatMsg.objects.filter(chat=sala):
            counter=counter+1
        array_msgs.append(counter)
    return array_msgs

def get_fav_list(request):
    fav_list = []
    cookie = request.COOKIES.get("session")
    for fav in LikedChats.objects.all():
        if fav.session.session == cookie:
            fav_list.append(fav)
    return fav_list


def check_fav(request):
    fav = []
    fav_list_chat = []
    fav_list = get_fav_list(request)
    for favour in fav_list:
        fav_list_chat.append(favour.chat)
    chat_list = get_chat_list()
    if len(fav_list) == 0:
        for _ in chat_list:
            fav.append(False)
    else:
        for chat in chat_list:
            if chat in fav_list_chat:
                fav.append(True)
            else:
                fav.append(False)
    return fav

def sesion_check(request, answer):
    cookie = request.COOKIES.get("session")
    if cookie is None:
        answer.set_cookie("session", ''.join(random.choices(string.ascii_lowercase + string.digits, k=128)))
    if request.path != "/login":
        check = Session.objects.filter(session=cookie)
    else:
        check = True
    return check

def xml_parser(xml_string):
    root = ET.fromstring(xml_string)
    messages = []
    for msg in root.findall('message'):
        isimg = msg.get('isimg')
        if isimg == "True":
            is_img = True
        else:
            is_img = False
        text = msg.find('text').text
        messages.append({'isimg': is_img, 'text': text})
    return messages

def css_generator(font, background_color,font_size):
    css_content = f'''.body {{
        font-family: {font};
        background-color: {background_color};
        font-size : {font_size}rem;
    }}'''
    return css_content

@ensure_csrf_cookie
def login(request):
    if request.method == "POST":
        psswrd = request.POST["password"]
        check = Passwords.objects.filter(password=psswrd)
        if check:
            session = Session(session=request.COOKIES.get("session"))
            session.save()
            return HttpResponseRedirect("/")
    return render(request, "Login.html")


@ensure_csrf_cookie
def get_home(request):
    login_html = HttpResponseRedirect("/login")
    if not sesion_check(request, login_html):
        return login_html
    c_msg, c_img, c_salas = get_footer_nums(request)
    chat_hour = get_chathour(request)
    total_msg = get_totalchatmsg(request)
    msg_notread = get_notread(request)
    cookie = request.COOKIES.get("session")
    session = Session.objects.get(session=cookie)
    if request.method == "POST":
        favourite = request.POST.get("favourite")
        chat = Chat.objects.get(id=favourite)
        check_fav_chat = LikedChats.objects.filter(session=session, chat=chat)
        if not check_fav_chat:
            fav_chat = LikedChats(session=session, chat=chat)
            fav_chat.save()
        else:
            unfav_chat = LikedChats.objects.get(session=session, chat=chat)
            unfav_chat.delete()

    n_salas = len(get_chat_list())
    favs = check_fav(request)
    comb = zip(get_chat_list(), msg_notread, chat_hour, total_msg,favs)
    name = get_username(request)
    home_html = render(request, "Home.html",
                       {"chat_list": get_chat_list(), "combinacion": comb, "c_msg": c_msg, "c_img": c_img,
                        "c_salas": c_salas, "name":name, "style_css": css_generator(session.font,session.background,session.font_size),"n_salas":n_salas})
    return home_html

@ensure_csrf_cookie
def get_favs(request):
    login_html = HttpResponseRedirect("/login")
    if not sesion_check(request, login_html):
        return login_html
    c_msg, c_img, c_salas = get_footer_nums(request)
    chat_hour = get_chathour(request)
    total_msg = get_totalchatmsg(request)
    msg_notread = get_notread(request)
    cookie = request.COOKIES.get("session")
    session = Session.objects.get(session=cookie)
    if request.method == "POST":
        favourite = request.POST.get("favourite")
        chat = Chat.objects.get(id=favourite)
        check_fav_chat = LikedChats.objects.filter(session=session, chat=chat)
        if not check_fav_chat:
            fav_chat = LikedChats(session=session, chat=chat)
            fav_chat.save()
    for i in get_fav_list(request):
        print(i.chat.name)
    n_salas_like = len(get_chat_list())
    favs = check_fav(request)
    comb = zip(get_fav_list(request), msg_notread, chat_hour, total_msg,favs)
    name = get_username(request)
    favs_html = render(request, "Favs.html",
                       {"chat_list": get_fav_list(request), "combinacion": comb, "c_msg": c_msg, "c_img": c_img,
                        "c_salas": c_salas, "name":name, "style_css": css_generator(session.font,session.background,session.font_size),"n_salas_like":n_salas_like})
    return favs_html


@ensure_csrf_cookie
def logout(request):
    login_html = HttpResponseRedirect("/login")
    if not sesion_check(request, login_html):
        return login_html
    cookie = request.COOKIES.get("session")
    session = Session.objects.get(session=cookie)
    c_msg, c_img, c_salas = get_footer_nums(request)
    if cookie is None:
        answer = HttpResponseRedirect("/login")
        answer.set_cookie("session", ''.join(random.choices(string.ascii_lowercase + string.digits, k=128)))
        return answer

    check = Session.objects.filter(session=cookie)
    if not check:
        return HttpResponseRedirect("/login")

    if request.method == "POST":
        session = Session.objects.get(session=cookie)
        session.delete()
        return HttpResponseRedirect("/login")
    name = get_username(request)
    return render(request, "Logout.html",{"c_msg":c_msg,"c_img":c_img,"c_salas":c_salas,"name":name,"style_css": css_generator(session.font,session.background,session.font_size)})



def get_help(request):
    login_html = HttpResponseRedirect("/login")
    if not sesion_check(request, login_html):
        return login_html
    c_msg, c_img, c_salas = get_footer_nums(request)
    login_html = HttpResponseRedirect("/login")
    name = get_username(request)
    cookie = request.COOKIES.get("session")
    session = Session.objects.get(session=cookie)
    help_html = render(request, "Help.html", {"chat_list": get_chat_list(),"c_msg":c_msg,"c_img":c_img,"c_salas":c_salas,"name":name,"style_css": css_generator(session.font,session.background,session.font_size)})
    if not sesion_check(request, login_html):
        return login_html

    return help_html


@ensure_csrf_cookie
def get_configuration(request):
    login_html = HttpResponseRedirect("/login")
    if not sesion_check(request, login_html):
        return login_html
    c_msg, c_img, c_salas = get_footer_nums(request)
    cookie = request.COOKIES.get("session")
    login_html = HttpResponseRedirect("/login")
    name = get_username(request)

    if not sesion_check(request, login_html):
        return login_html
    session = Session.objects.get(session=cookie)
    if request.method == "POST":
        form = request.POST
        if "name" in form:
            name = request.POST["name"]
            session.name = name
        if "font" in form:
            font = request.POST["font"]
            session.font = font
        if "background" in form:
            background = request.POST["background"]
            session.background = background
        if "font_size" in form:
            font_size = request.POST["font_size"]
            session.font_size = float(font_size)
        session.save()
    configuration_html = render(request, "Configuration.html",
                                {"chat_list": get_chat_list(), "c_msg": c_msg, "c_img": c_img, "c_salas": c_salas,
                                 "name": name, "style_css": css_generator(session.font,session.background,session.font_size)})
    return configuration_html

@ensure_csrf_cookie
def create_chat(request):
    login_html = HttpResponseRedirect("/login")
    if not sesion_check(request, login_html):
        return login_html
    c_msg, c_img, c_salas = get_footer_nums(request)
    cookie = request.COOKIES.get("session")
    session = Session.objects.get(session=cookie)
    login_html = HttpResponseRedirect("/login")
    name = get_username(request)
    create_chat_html = render(request, "CreateChat.html", {"chat_list": get_chat_list(),"c_msg":c_msg,"c_img":c_img,"c_salas":c_salas,"name":name,"style_css": css_generator(session.font,session.background,session.font_size)})
    if not sesion_check(request, login_html):
        return login_html
    session = Session.objects.get(session=cookie)
    if request.method == "POST":
        name = request.POST["name"]
        description = request.POST["description"]
        check_name = Chat.objects.filter(name=name)
        if not check_name:
            chat = Chat(name=name, creator=session, description=description)
            chat.save()
            chat_html = HttpResponseRedirect("/" + name)
            return chat_html
    return create_chat_html


@ensure_csrf_cookie
def chat(request, chat_name):
    login_html = HttpResponseRedirect("/login")
    if not sesion_check(request, login_html):
        return login_html
    home_html = HttpResponseRedirect("/")
    try:
        chat = Chat.objects.get(name=chat_name)
        cookie = request.COOKIES.get("session")
        login_html = HttpResponseRedirect("/login")
        session = Session.objects.get(session=cookie)
        chat_list = get_chat_list()
        visited = VisitedChats.objects.filter(chat=chat,session=session)
        if not visited:
            visit = VisitedChats(session=session, chat=chat)
            visit.save()
        else :
            visit = VisitedChats.objects.get(chat=chat,session=session)
            visit.access_date = timezone.localtime(timezone.now())
            visit.save()
        if not sesion_check(request, login_html):
            return login_html
        if request.method == "POST":
            form = request.POST
            if "message" in form:
                msg = request.POST.get("message")
                send_value = request.POST.get("send")
                if send_value == "True":
                    is_img = True
                else:
                    is_img = False
                chat_msg = ChatMsg(session=session, name=session.name, msg=msg, chat=chat, is_img=is_img)
                chat_msg.save()
        elif request.method == "PUT":
            xml_string = request.body.decode('utf-8')
            msg_xml = xml_parser(xml_string)
            for msg in msg_xml:
                chat_msg = ChatMsg(session=session, name=session.name, msg=msg['text'], chat=chat, is_img=msg['isimg'])
                chat_msg.save()
        msg_list = get_msg_list(request)
        name = get_username(request)
        c_msg, c_img, c_salas = get_footer_nums(request)
        return render(request, "Chat.html", {"msg_list": msg_list, "session": cookie, "chat_list": chat_list,"c_msg":c_msg,"c_img":c_img,"c_salas":c_salas,"name":name,"style_css": css_generator(session.font,session.background,session.font_size)})
    except Chat.DoesNotExist:
        return home_html


def get_json(request, chat_name):
    login_html = HttpResponseRedirect("/login")
    if not sesion_check(request, login_html):
        return login_html
    msg_list = []
    json_list = []
    cookie = request.COOKIES.get("session")
    for msg in ChatMsg.objects.all():
        if msg.chat.name == chat_name:
            msg_list.append(msg)
    for msg in msg_list:
        if msg.session is None:
            msg_json = {'author': msg.name, 'msg': msg.msg, 'is_img': msg.is_img,'date': msg.date.isoformat(),'session': "A"}
        else:
            msg_json = {'author': msg.name, 'msg': msg.msg, 'is_img': msg.is_img,'date': msg.date.isoformat(),'session': msg.session.session}
        json_list.append(msg_json)
    json_therealone = json.dumps(json_list)
    return HttpResponse(json_therealone, content_type='application/json')

@ensure_csrf_cookie
def dynamic_chat(request, chat_name):
    login_html = HttpResponseRedirect("/login")
    if not sesion_check(request, login_html):
        return login_html
    home_html = HttpResponseRedirect("/")
    try:
        chat = Chat.objects.get(name=chat_name)
        cookie = request.COOKIES.get("session")
        login_html = HttpResponseRedirect("/login")
        session = Session.objects.get(session=cookie)
        if not sesion_check(request, login_html):
            return login_html
        if request.method == "POST":
            form = request.POST
            if "message" in form:
                msg = request.POST.get("message")
                send_value = request.POST.get("send")
                if send_value == "True":
                    is_img = True
                else:
                    is_img = False
                chat_msg = ChatMsg(session=session, name=session.name, msg=msg, chat=chat, is_img=is_img)
                chat_msg.save()
        chat_list = get_chat_list()
        url_json = "/" + chat_name + ".json"
        c_msg, c_img, c_salas = get_footer_nums(request)
        name = get_username(request)
        return render(request, "ChatDinamico.html",
                      {"session": cookie, "chat_list": chat_list, "c_msg": c_msg, "c_img": c_img,
                       "c_salas": c_salas, "name": name, "url_json":url_json, "style_css":css_generator(session.font,session.background,session.font_size)})
    except Chat.DoesNotExist:
        return home_html






