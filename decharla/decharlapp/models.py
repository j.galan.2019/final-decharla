from datetime import datetime

from django.db import models
from django.utils import timezone

class Passwords(models.Model):
    password = models.TextField()

class Session(models.Model):
    session = models.TextField(unique=True)
    name = models.TextField(default="Anonymus")
    font = models.TextField()
    background = models.TextField()
    font_size = models.FloatField(default=1)

class Chat(models.Model):
    name = models.TextField()
    creator = models.ForeignKey(Session, on_delete=models.SET_NULL, null=True)
    description = models.TextField()


class ChatMsg(models.Model):
    session = models.ForeignKey(Session, on_delete=models.SET_NULL, null=True)
    name = models.TextField()
    msg = models.TextField()
    chat = models.ForeignKey(Chat, on_delete=models.CASCADE)
    date = models.DateTimeField(default=timezone.now)
    is_img = models.BooleanField(default=False)

class VisitedChats(models.Model):
    access_date = models.DateTimeField(default=timezone.now)
    session = models.ForeignKey(Session, on_delete=models.CASCADE)
    chat = models.ForeignKey(Chat, on_delete=models.CASCADE)

class LikedChats(models.Model):
    session = models.ForeignKey(Session, on_delete=models.CASCADE)
    chat = models.ForeignKey(Chat, on_delete=models.CASCADE)
    fav = models.BooleanField(default=False)












